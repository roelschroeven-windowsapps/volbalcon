//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <RsUtil.h>

#include "PromptForPresetName.h"
#include "Presets.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormPromptForPresetName *FormPromptForPresetName;
//---------------------------------------------------------------------------

bool PromptForPresetName(const TPresets &Presets, const String &CurrentPresetName, String &NewPresetName)
{
  std::auto_ptr<TFormPromptForPresetName> Form = rsutil::CreateVclForm<TFormPromptForPresetName>();
  Form->Set(Presets, CurrentPresetName);
  if (Form->ShowModal() != mrOk)
    return false;
  NewPresetName = Form->GetPresetName();
  return true;
}
//---------------------------------------------------------------------------

__fastcall TFormPromptForPresetName::TFormPromptForPresetName(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void TFormPromptForPresetName::Set(const TPresets &Presets, const String &Name)
{
  m_pPresets = &Presets;
  for (std::map<String, TVolumeSetting>::const_iterator it = m_pPresets->VolumeSettings.begin(); it != m_pPresets->VolumeSettings.end(); ++it)
    ComboBoxPresetName->Items->Append(it->first);
  ComboBoxPresetName->Text = Name;
}

String TFormPromptForPresetName::GetPresetName()
{
  return ComboBoxPresetName->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFormPromptForPresetName::ButtonOkClick(TObject *Sender)
{
  if (ComboBoxPresetName->Text.IsEmpty())
    return;
  if (m_pPresets->VolumeSettings.count(ComboBoxPresetName->Text) > 0)
    {
    if (ID_YES != Application->MessageBox(
                   rsutil::PrintF(L"Overwrite existing preset '%ls'?", ComboBoxPresetName->Text.c_str()).c_str(),
                   L"Preset already exists",
                   MB_YESNO))
      return;
    }
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
