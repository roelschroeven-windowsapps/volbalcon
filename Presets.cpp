//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <Registry.hpp>
#include <RsUtil.h>

#include "Presets.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

const String BaseKey = "\\Software\\Roel Schroeven\\VolBalCon";
const String PresetsKey = "\\Software\\Roel Schroeven\\VolBalCon\\Presets";

using rsutil::JoinPaths;
using rsutil::PrintF;

void TPresets::Load()
{
  std::auto_ptr<TRegistry> Registry(new TRegistry);
  Registry->RootKey = HKEY_CURRENT_USER;
  if (!Registry->OpenKeyReadOnly(PresetsKey))
    return;
  std::auto_ptr<TStrings> PresetNames(new TStringList);
  Registry->GetKeyNames(PresetNames.get());
  for (int i = 0; i < PresetNames->Count; ++i)
    {
    String Name = PresetNames->Strings[i];
    if (!Registry->OpenKeyReadOnly(JoinPaths(PresetsKey, Name)))
      continue;
    TVolumeSetting VolumeSetting;
    VolumeSetting.Master = 0.01 * Registry->ReadInteger("Master");
    int ChannelIndex = 0;
    while (true)
      {
      String ValueName = PrintF(L"Channel%d", ChannelIndex);
      if (!Registry->ValueExists(ValueName))
        break;
      float ChannelVolume = 0.0;
      try { ChannelVolume = 0.01 * Registry->ReadInteger(ValueName); }
      catch (Exception &E) {  }
      VolumeSetting.Channels.push_back(ChannelVolume);
      ChannelIndex += 1;
      }
    VolumeSettings[Name] = VolumeSetting;
    }
}

static void DeletePresetsFromRegistry(TRegistry *Registry, const String &PresetsKey)
{
  if (!Registry->OpenKey(PresetsKey, false))
    return;
  std::auto_ptr<TStrings> PresetNames(new TStringList);
  Registry->GetKeyNames(PresetNames.get());
  for (int i = 0; i < PresetNames->Count; ++i)
    Registry->DeleteKey(JoinPaths(PresetsKey, PresetNames->Strings[i]));
}

void TPresets::Save()
{
  std::auto_ptr<TRegistry> Registry(new TRegistry);
  Registry->RootKey = HKEY_CURRENT_USER;

  // First remove all presets to start from a clean slate
  DeletePresetsFromRegistry(Registry.get(), PresetsKey);

  // Write presets
  for (std::map<String, TVolumeSetting>::const_iterator it = VolumeSettings.begin(); it != VolumeSettings.end(); ++it)
    {
    const String &PresetName = it->first;
    const TVolumeSetting &VolumeSetting = it->second;
    if (!Registry->OpenKey(JoinPaths(PresetsKey, PresetName), true))
      continue;
    Registry->WriteInteger(L"Master", 100 * VolumeSetting.Master);
    for (unsigned i = 0; i < VolumeSetting.Channels.size(); ++i)
      Registry->WriteInteger(PrintF(L"Channel%d", i), 100 * VolumeSetting.Channels[i]);
    }
}

