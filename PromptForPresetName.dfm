object FormPromptForPresetName: TFormPromptForPresetName
  Left = 0
  Top = 0
  Caption = 'Save preset'
  ClientHeight = 116
  ClientWidth = 246
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 246
    Height = 75
    Align = alClient
    ShowCaption = False
    TabOrder = 0
    ExplicitLeft = 280
    ExplicitTop = 16
    ExplicitWidth = 185
    ExplicitHeight = 41
    DesignSize = (
      246
      75)
    object Label1: TLabel
      Left = 12
      Top = 16
      Width = 114
      Height = 13
      Caption = 'Save preset with name:'
    end
    object ComboBoxPresetName: TComboBox
      Left = 12
      Top = 35
      Width = 225
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
      ExplicitWidth = 434
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 75
    Width = 246
    Height = 41
    Align = alBottom
    ShowCaption = False
    TabOrder = 1
    ExplicitLeft = 368
    ExplicitTop = 72
    ExplicitWidth = 185
    DesignSize = (
      246
      41)
    object ButtonOk: TButton
      Left = 81
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'OK'
      Default = True
      TabOrder = 0
      OnClick = ButtonOkClick
    end
    object ButtonCancel: TButton
      Left = 162
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight, akBottom]
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
end
