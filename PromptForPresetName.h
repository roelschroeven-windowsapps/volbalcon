//---------------------------------------------------------------------------

#ifndef PromptForPresetNameH
#define PromptForPresetNameH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------

class TPresets;

bool PromptForPresetName(const TPresets &Presets, const String &CurrentPresetName, String &NewPresetName);

class TFormPromptForPresetName : public TForm
{
__published:	// IDE-managed Components
  TPanel *Panel1;
  TPanel *Panel2;
  TLabel *Label1;
  TComboBox *ComboBoxPresetName;
  TButton *ButtonOk;
  TButton *ButtonCancel;
  void __fastcall ButtonOkClick(TObject *Sender);

private:	// User declarations
  const TPresets *m_pPresets;

public:		// User declarations
  __fastcall TFormPromptForPresetName(TComponent* Owner);

  void Set(const TPresets &Presets, const String &Name);
  String GetPresetName();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormPromptForPresetName *FormPromptForPresetName;
//---------------------------------------------------------------------------
#endif
